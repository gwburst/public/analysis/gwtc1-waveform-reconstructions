
#  -----------------------------------------------------------------
# init GWTC-1 waveform reconstructions environment
#  -----------------------------------------------------------------

  # init ligo skymap
  if [[ -e ~/virtualenv/ligo.skymap/bin/activate ]]; then
    source ~/virtualenv/ligo.skymap/bin/activate
    export PYTHONPATH=""
  else
    echo '~/virtualenv/ligo.skymap/bin/activate doesn''t exist'
    echo ''
    echo ' please install ligo.skymap package:'
    echo ''
    echo ' python3 -m venv ~/virtualenv/ligo.skymap'
    echo ' source ~/virtualenv/ligo.skymap/bin/activate'
    echo ' pip3 install ligo.skymap'
    echo ''
  fi

#  -----------------------------------------------------------------
#  DO NOT MODIFY !!!
#
#  1) In this section the HOME_WFR env is automatically initialized 
#  2) Init Default cWB library 
#  3) Init Default cWB config  
#  -----------------------------------------------------------------

  MYSHELL=`readlink /proc/$$/exe`
  if [[  "$MYSHELL" =~ "tcsh"  ]]; then
    echo "\nEntering in TCSH section..."
     CWB_CALLED=($_)
    if [[=$#CWB_CALLED = 2 ]]; then # alias
      set CWB_CALLED=`alias=$CWB_CALLED`
    fi
    if [[  "$CWB_CALLED" != ""  ]]; then
       WATENV_SCRIPT=`readlink -f $CWB_CALLED[2]`
    else
      echo "\nError: script must be executed with source command"
      return 0 1
    fi
     script_dir=`dirname $WATENV_SCRIPT`
  fi
  if [[  "$MYSHELL" =~ "bash"  ]]; then
    echo ""
    echo "Entering in BASH section..."
    script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) #_SKIP_CSH2SH_
  fi
  if [[  "$MYSHELL" =~ "zsh"  ]]; then
    echo "\nEntering in ZSH section..."
    script_dir=$( cd "$( dirname "${(%):-%N}" )" && pwd )        #_SKIP_CSH2SH_
  fi
  export HOME_WFR=$script_dir

  echo ''
  echo ' GWTC-1 Waveform Reconstructions Init ...'
  echo ' HOME_WFR = '$HOME_WFR
  echo ''

#  -----------------------------------------------------------------
# more setup
#  -----------------------------------------------------------------

  alias xmake='make -f $HOME_WFR/Makefile'


