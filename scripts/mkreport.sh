#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("GW150914" "GW151012" "GW151226" "GW170104" "GW170608" "GW170729" "GW170809" "GW170814" "GW170818" "GW170823" "$CWB_GWNAME")

GWTC1_GIT_URL="https://gitlab.com/gwburst/public/analysis/gwtc1-waveform-reconstructions"

if [ "$1" == '' ] || [ "$2" == '' ]; then
  echo ''
  echo 'mkreport.sh par1 par2 par3 par4 par5'
  echo ''
  echo ' par1: available options are "all" or ...'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' par2: available options: time/envelope/frequency/psd/white/residuals/event'
  echo ''
  echo ' par3: available options: plot/html/all'
  echo ''
  echo '  plot    -> data fits files are converted to png'
  echo '  html    -> produce html report'
  echo '  all     -> plot+html'
  echo ''
  echo '  if par2=event -> by default par3=html'
  echo ''
  echo ' par4: (optional):'
  echo ''
  echo '  if par2=time/envelope/frequency/white/residuals & par3=plot: it is used to provide plot user input parameters'
  echo ''
  echo '    format: {"name",offset,min,max,inf,sup,"leg",lwidth}    # white spaces are not allowed'
  echo ''
  echo '      name          // gw name: Ex: GW150914'
  echo '      offset        // GPS offset'
  echo '      min           // begin of the plot time wrt offset'
  echo '      max           // end of the plot time wrt offset'
  echo '      inf           // min of y axis'
  echo '      sup           // max of y axis'
  echo '      leg           // legend position (down-left, down-right, up-left, up-right)'
  echo '      lwidth        // line width'
  echo ''
  echo '     ex: '{"\"GW190521"\",1242442967.0,0.30,0.60,-3.5,3.5,"\"down-right\"",4}' '
  echo ''
  echo '  if par2=event & par3=html: it is used to provide the subtitle to event html page'
  echo ''
  echo '     ex: ''( LALInference approximant = NRSur7dq4 )'' '
  echo ''
  echo ' par5: (optional): available options: true/false(default)'
  echo ''
  echo '       if true the index.html is modified in order to be used for public pages'
  echo ''
  exit 0
fi

# check if cWB is installed
$HOME_CWB/scripts/cwb_watenv.sh
if [ $? != 0 ]; then echo ''; echo 'error: cWB must be installed !!! process terminated'; echo ''; exit 1; fi

TYPE=''
if [ "$2" != '' ]; then
  TYPE=$2
fi

OPTIONS=''
if [ "$3" == 'plot' ]; then
  OPTIONS='{"",0.0,0.0,0.0,0.0,0.0,"",0}'
fi
if [ "$4" != '' ]; then
  OPTIONS=$4
fi
if [ "$3" == 'plot' ]; then
  if [[ "$OPTIONS" =~ ' ' ]]; then
    echo ''
    echo 'Error: the input parameter '"'$OPTIONS'"' must not contains white spaces'
    echo ''  
    exit 1
 fi
fi
if [ "$2" == 'event' ] && [ "$3" == 'html' ]; then
  DEFAULT_OPTIONS='{"",0.0,0.0,0.0,0.0,0.0,"",0}'
  if [ "$OPTIONS" == $DEFAULT_OPTIONS ]; then
    OPTIONS=''
  fi
fi

PUBLIC_INDEX='false'
if [ "$5" != '' ]; then
  PUBLIC_INDEX=$5
fi

if [ "$TYPE" != 'time' ] && [ "$TYPE" != 'envelope' ] && [ "$TYPE" != 'frequency' ] && [ "$TYPE" != 'psd' ] && [ "$TYPE" != 'white' ] && [ "$TYPE" != 'residuals' ] && [ "$TYPE" != 'event' ]; then
  echo ''
  echo 'input par2 not available option'
  echo ''
  echo ' par2: available options: time/envelope/frequency/psd/white/residuals/event'
  echo ''
  exit 0
fi

HOME_WWW=$(echo "$HOME_WWW" | sed "s/~/\/~/")
HOME_WWW=$(echo "$HOME_WWW" | sed "s/\//\\\\\//g")
if [ "$TYPE" == 'all' ] || [ "$TYPE" == 'event' ]; then
  for gwname in ${GW_LIST[@]} ; do
    if [ "$1" == 'all' ] || [ "$1" == $gwname ]; then
      #cd 'events/'$gwname'/report'; rm -f all; ln -sf ../../../html/event -r all; cd -
      if [ ! -d "events/$gwname/report/all" ]; then mkdir -p "events/$gwname/report/all"; fi; 
      rm -f 'events/'$gwname'/report/all/*';
      cp ''$HOME_WFR'/html/event/ROOT.css'   'events/'$gwname'/report/all/.';
      cp ''$HOME_WFR'/html/event/ROOT.js'    'events/'$gwname'/report/all/.';
      cp ''$HOME_WFR'/html/event/tabber.css' 'events/'$gwname'/report/all/.';
      cp ''$HOME_WFR'/html/event/tabber.js'  'events/'$gwname'/report/all/.';
      cp ''$HOME_WFR'/html/event/index.html' 'events/'$gwname'/report/all/.';
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
      sed -i 's/HOME_WWW/'"$HOME_WWW"'/g' events/$gwname/report/all/index.html
      if [ $PUBLIC_INDEX == 'true' ]; then
        # the index.html is modified in order to be used for public pages
        ln -sf ../../../html  -t  events/"$gwname"/report/
        ln -sf ../../html  -t  events/"$gwname"/
        sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' events/$gwname/report/all/index.html
      fi
      #OPTIONS=$(echo "${OPTIONS}" | sed "s/#/ /g") # subtitute # with white spaces
      OPTIONS_SAVE=$OPTIONS;	# save options
      GWTC1_GIT_REPO_URL=''$GWTC1_GIT_URL'/-/tree/public/events/'$gwname'/data'
      GWTC1_GIT_INFO_URL=''$GWTC1_GIT_URL'/-/blob/public/README.md'
      OPTIONS="reconstructed <a href='"$GWTC1_GIT_REPO_URL"'>data</a> (<a href='"$GWTC1_GIT_INFO_URL"'>README</a>) - LALInference model = IMRPhenomPv2"
      OPTIONS=$(echo "${OPTIONS}" | sed "s/\//\\\\\//g")
      sed -i 's/<!---SUBTITLE-->/'"$OPTIONS"'/' events/"$gwname"/report/all/index.html
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
      OPTIONS=$OPTIONS_SAVE;	# restore options
    fi
  done
  exit 0
fi

if [ "$3" != 'plot' ] && [ "$3" != 'html' ] && [ "$3" != 'all' ]; then
  echo ''
  echo 'input par3 not available option'
  echo ''
  echo ' par3: available options: plot/html/all'
  echo ''
  exit 0
fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then  
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

if [ "$3" == 'all' ] || [ "$3" == 'plot' ]; then

  # create report directories
  for gwname in ${GW_LIST[@]} ; do
    if [ "$1" == 'all' ] || [ "$1" == $gwname ]; then
      if [ ! -d "events/$gwname/report/$TYPE" ]; then mkdir -p "events/$gwname/report/$TYPE"; fi; 
    fi
  done

  # create plots

  if [ "$TYPE" == 'time' ] || [ "$TYPE" == 'envelope' ] || [ "$TYPE" == 'white' ] || [ "$TYPE" == 'residuals' ] || [ "$TYPE" == 'frequency' ]; then
    for gwname in ${GW_LIST[@]} ; do
      if [ "$1" == 'all' ] || [ "$1" == $gwname ]; then
	if [[ $TYPE == 'residuals' ]]; then LABEL="time"; else LABEL=$TYPE; fi; 
        for f in $(ls -1 events/"$gwname"/data/*"$LABEL"*.dat | sort --ignore-case) ; do
          if [[ $f == *"H1"* ]]; then IFO="H1"; fi;
          if [[ $f == *"L1"* ]]; then IFO="L1"; fi;
          if [[ $f == *"V1"* ]]; then IFO="V1"; fi;
          cmd='root -l -b -q '''$HOME_WFR'/macros/DrawEventWaveforms.C("'$gwname'","'$IFO'","'$TYPE'",0.0,0.0,'$OPTIONS')'''
          echo $cmd; $cmd
          if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
        done
      fi
    done
  fi

  if [ "$TYPE" == 'time' ] || [ "$TYPE" == 'envelope' ] || [ "$TYPE" == 'white' ] || [ "$TYPE" == 'residuals' ]; then

    if [ "$1" == 'all' ] || [ "$1" == "GW151012" ]; then
      # zoom
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW151012","L1","'$TYPE'",0.20,1.00)'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW151012","H1","'$TYPE'",0.20,1.00)'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
    fi

    if [ "$1" == 'all' ] || [ "$1" == "GW151226" ]; then
      # zoom
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW151226" ,"L1","'$TYPE'",0.20,0.80)'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW151226" ,"H1","'$TYPE'",0.20,0.80)'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
    fi

    if [ "$1" == 'all' ] || [ "$1" == "GW170608" ]; then
      # zoom
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW170608" ,"L1","'$TYPE'",1.25,1.6)'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW170608" ,"H1","'$TYPE'",1.25,1.6)'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
    fi

    if [ "$1" == 'all' ] || [ "$1" == "GW170729" ]; then
      # zoom
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW170729" ,"L1","'$TYPE'",0.1,0.8,{"GW170729",1185389807.,0.1,0.8,-2.5,2.5,"down-right",4})'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW170729" ,"H1","'$TYPE'",0.1,0.8,{"GW170729",1185389807.,0.1,0.8,-2.5,2.5,"down-right",4})'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
    fi

    if [ "$1" == 'all' ] || [ "$1" == "GW170814" ]; then
      # zoom
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW170814" ,"L1","'$TYPE'",0.2,0.8)'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
      root -l -b -q ''$HOME_WFR'/macros/DrawEventWaveforms.C("GW170814" ,"H1","'$TYPE'",0.2,0.8)'
      if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
    fi
  fi

  if [ "$TYPE" == 'psd' ]; then
      for gwname in ${GW_LIST[@]} ; do
      if [ "$1" == 'all' ] || [ "$1" == $gwname ]; then
        cmd='root -l -b -q '''$HOME_WFR'/macros/DrawEventPSD.C("'$gwname'")'''
        echo $cmd; $cmd
        if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
      fi
    done
  fi
fi

if [ "$3" == 'all' ] || [ "$3" == 'html' ]; then

  # check if report directory exist
  for gwname in ${GW_LIST[@]} ; do
    if [ "$1" == 'all' ] || [ "$1" == $gwname ]; then
      if [ ! -d "events/$gwname/report/$TYPE" ]; then 
        echo "Error: report directory events/$gwname/report/$TYPE doesn't exist"
        exit 1
      fi;
    fi
  done

  # make html plots
  for gwname in ${GW_LIST[@]} ; do
    if [ "$1" == 'all' ] || [ "$1" == $gwname ]; then
      if [ "$TYPE" == 'time' ]; then
        $HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE" '--title '$gwname'#(Time#Domain)'' --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

        COUNTER=1
        # we sorted to be compatible with the sorting used by the cwb_mkhtml command 
        for f in $(ls -1 events/"$gwname"/report/$TYPE/*.png | sort --ignore-case) ; do
          ofile=$(echo "${f}" | sed "s/.fits/.png/")
          label="${ofile##*/}"
          #if [ "$label" == "rec_signal_time_H1.png"      ]; then label="Time Domain";        fi
          #if [ "$label" == "rec_signal_time_H1_zoom.png" ]; then label="Time Domain (zoom)"; fi
          #if [ "$label" == "rec_signal_time_L1.png"      ]; then label="Time Domain";        fi
          #if [ "$label" == "rec_signal_time_L1_zoom.png" ]; then label="Time Domain (zoom)"; fi
          #sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'"$label"'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'""'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          COUNTER=$[$COUNTER +1]
        done

        if [ $PUBLIC_INDEX == 'true' ]; then
          # the index.html is modified in order to be used for public pages
          ln -sf ../../../html  -t  events/"$gwname"/report/
          ln -sf ../../html  -t  events/"$gwname"/
          sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' events/"$gwname"/report/$TYPE/png_html_index/index.html
        fi
      fi
      if [ "$TYPE" == 'envelope' ]; then
        $HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE" '--title '$gwname'#(Time#Domain#-#Envelope)'' --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

        COUNTER=1
        # we sorted to be compatible with the sorting used by the cwb_mkhtml command 
        for f in $(ls -1 events/"$gwname"/report/$TYPE/*.png | sort --ignore-case) ; do
          ofile=$(echo "${f}" | sed "s/.fits/.png/")
          label="${ofile##*/}"
          #if [ "$label" == "rec_signal_envelope_H1.png"      ]; then label="Time Domain";        fi
          #if [ "$label" == "rec_signal_envelope_H1_zoom.png" ]; then label="Time Domain (zoom)"; fi
          #if [ "$label" == "rec_signal_envelope_L1.png"      ]; then label="Time Domain";        fi
          #if [ "$label" == "rec_signal_envelope_L1_zoom.png" ]; then label="Time Domain (zoom)"; fi
          #sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'"$label"'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'""'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          COUNTER=$[$COUNTER +1]
        done

        if [ $PUBLIC_INDEX == 'true' ]; then
          # the index.html is modified in order to be used for public pages
          ln -sf ../../../html  -t  events/"$gwname"/report/
          ln -sf ../../html  -t  events/"$gwname"/
          sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' events/"$gwname"/report/$TYPE/png_html_index/index.html
        fi
      fi
      if [ "$TYPE" == 'frequency' ]; then
        $HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE" '--title '$gwname'#(Frequency#Domain)'' --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

        COUNTER=1
        # we sorted to be compatible with the sorting used by the cwb_mkhtml command 
        for f in $(ls -1 events/"$gwname"/report/$TYPE/*.png | sort --ignore-case) ; do
          ofile=$(echo "${f}" | sed "s/.fits/.png/")
          label="${ofile##*/}"
          #if [ "$label" == "rec_signal_frequency_H1.png"      ]; then label="Frequency Domain";        fi
          #if [ "$label" == "rec_signal_frequency_L1.png"      ]; then label="Frequency Domain";        fi
          #sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'"$label"'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'""'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          COUNTER=$[$COUNTER +1]
        done

        if [ $PUBLIC_INDEX == 'true' ]; then
          # the index.html is modified in order to be used for public pages
          ln -sf ../../../html  -t  events/"$gwname"/report/
          ln -sf ../../html  -t  events/"$gwname"/
          sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' events/"$gwname"/report/$TYPE/png_html_index/index.html
        fi
      fi
      if [ "$TYPE" == 'psd' ]; then
        $HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE" '--title '$gwname'#(Strain#Sensitivities)'
        #$HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE"

        COUNTER=1
        # we sorted to be compatible with the sorting used by the cwb_mkhtml command 
        for f in $(ls -1 events/"$gwname"/report/$TYPE/*.png | sort --ignore-case) ; do
          ofile=$(echo "${f}" | sed "s/.fits/.png/")
          label="${ofile##*/}"
          if [ "$label" == "H1_L1_psd.png" ]; then label="$gwname"; fi
          #sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'"$label"'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'""'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          COUNTER=$[$COUNTER +1]
        done

        if [ $PUBLIC_INDEX == 'true' ]; then
          # the index.html is modified in order to be used for public pages
          ln -sf ../../../html  -t  events/"$gwname"/report/
          ln -sf ../../html  -t  events/"$gwname"/
          sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' events/"$gwname"/report/$TYPE/png_html_index/index.html
        fi
      fi
      if [ "$TYPE" == 'white' ]; then
        # change temporary the name of frequency plots in order to visualize frequecy plot as the last one (cwb_mkhtml reports plots in alphabetic order)
        for f in $(ls -1 events/"$gwname"/report/$TYPE/*.png | sort --ignore-case) ; do
          ofile=$(echo "${f}" | sed "s/white_frequency_/zzz.white_frequency_/")
          if [ ${f} != ${ofile} ]; then mv ${f} ${ofile}; fi
        done
        $HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE" '--title '$gwname' --subtitle (cWB#max-likelihood)#vs#(LALInference#max-likelihood)#vs#(whitened#data#16:512#Hz)'
        #$HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE" '--title '$gwname' --subtitle (cWB#max-likelihood)#vs#(whitened#data#16:512#Hz)'

        COUNTER=1
        # we sorted to be compatible with the sorting used by the cwb_mkhtml command 
        for f in $(ls -1 events/"$gwname"/report/$TYPE/*.png | sort --ignore-case) ; do
          ofile=$(echo "${f}" | sed "s/.fits/.png/")
          label="${ofile##*/}"
          if [ "$label" == "white_time_H1.png"      ]; then label="Time Domain";        fi
          if [ "$label" == "white_time_H1_zoom.png" ]; then label=""; fi
          if [ "$label" == "white_time_L1.png"      ]; then label=""; fi
          if [ "$label" == "white_time_L1_zoom.png" ]; then label=""; fi
          if [ "$label" == "white_time_V1.png"      ]; then label=""; fi
          if [ "$label" == "white_time_V1_zoom.png" ]; then label=""; fi
          if [ "$label" == "zzz.white_frequency_H1.png" ]; then label="Frequency Domain";   fi
          if [ "$label" == "zzz.white_frequency_L1.png" ]; then label=""; fi
          if [ "$label" == "zzz.white_frequency_V1.png" ]; then label=""; fi
          sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'"$label"'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          COUNTER=$[$COUNTER +1]

          # restore the name of frequency plots and fix name of frequency plots in index.html file
          sed -i 's/zzz.white_frequency_/white_frequency_/g'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          ofile=$(echo "${f}" | sed "s/zzz.white_frequency_/white_frequency_/")
          if [ ${f} != ${ofile} ]; then mv ${f} ${ofile}; fi
        done

        if [ $PUBLIC_INDEX == 'true' ]; then
          # the index.html is modified in order to be used for public pages
          ln -sf ../../../html  -t  events/"$gwname"/report/
          ln -sf ../../html  -t  events/"$gwname"/
          sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' events/"$gwname"/report/$TYPE/png_html_index/index.html
        fi
      fi
      if [ "$TYPE" == 'residuals' ]; then
        # change temporary the name of frequency plots in order to visualize frequecy plot as the last one (cwb_mkhtml reports plots in alphabetic order)
        for f in $(ls -1 events/"$gwname"/report/$TYPE/*.png | sort --ignore-case) ; do
          ofile=$(echo "${f}" | sed "s/residuals_frequency_/zzz.residuals_frequency_/")
          if [ ${f} != ${ofile} ]; then mv ${f} ${ofile}; fi
        done
        $HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE" '--title '$gwname' --subtitle residuals#(LALInference-cWB)#vs#residuals#(data-cWB)'
        #$HOME_CWB/scripts/cwb_mkhtml.csh "events/"$gwname"/report/$TYPE" '--title '$gwname' --subtitle residuals#(cWB)#vs#residuals#(data-cWB)'


        COUNTER=1
        # we sorted to be compatible with the sorting used by the cwb_mkhtml command 
        for f in $(ls -1 events/"$gwname"/report/$TYPE/*.png | sort --ignore-case) ; do
          ofile=$(echo "${f}" | sed "s/.fits/.png/")
          label="${ofile##*/}"
          if [ "$label" == "residuals_time_H1.png"      ]; then label="Time Domain";        fi
          if [ "$label" == "residuals_time_H1_zoom.png" ]; then label=""; fi
          if [ "$label" == "residuals_time_L1.png"      ]; then label=""; fi
          if [ "$label" == "residuals_time_L1_zoom.png" ]; then label=""; fi
          if [ "$label" == "residuals_time_V1.png"      ]; then label=""; fi
          if [ "$label" == "residuals_time_V1_zoom.png" ]; then label=""; fi
          if [ "$label" == "zzz.residuals_frequency_H1.png" ]; then label="Frequency Domain";   fi
          if [ "$label" == "zzz.residuals_frequency_L1.png" ]; then label=""; fi
          if [ "$label" == "zzz.residuals_frequency_V1.png" ]; then label=""; fi
          sed -i 's/<h2>'"$COUNTER"'<\/h2>/<h2>'"$label"'<\/h2>/'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          COUNTER=$[$COUNTER +1]

          # restore the name of frequency plots and fix name of frequency plots in index.html file
          sed -i 's/zzz.residuals_frequency_/residuals_frequency_/g'  events/"$gwname"/report/$TYPE/png_html_index/index.html
          ofile=$(echo "${f}" | sed "s/zzz.residuals_frequency_/residuals_frequency_/")
          if [ ${f} != ${ofile} ]; then mv ${f} ${ofile}; fi
        done

        if [ $PUBLIC_INDEX == 'true' ]; then
          # the index.html is modified in order to be used for public pages
          ln -sf ../../../html  -t  events/"$gwname"/report/
          ln -sf ../../html  -t  events/"$gwname"/
          sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' events/"$gwname"/report/$TYPE/png_html_index/index.html
        fi
      fi
    fi
  done
fi

exit 0

trap - INT
echo "One more, but Ctrl-C should work again."
ps T | grep root | awk '{print $1}' | xargs kill -9
exit 1

