#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("GW150914" "GW151012" "GW151226" "GW170104" "GW170608" "GW170729" "GW170809" "GW170814" "GW170818" "GW170823" "$CWB_GWNAME")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkcopy.sh par1'
  echo ''
  echo ' par1 (optional) available options ...'
  echo ''
  echo ' default -> all'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  exit 0
fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

# copy GWTC-1 cwb skymap fits files
for gwname in ${GW_LIST[@]} ; do

  net="HL";
  if [ "$gwname" == 'GW170729' ]; then net='HLV';	fi;
  if [ "$gwname" == 'GW170809' ]; then net='HLV';	fi;
  if [ "$gwname" == 'GW170814' ]; then net='HLV';	fi;
  if [ "$gwname" == 'GW170817' ]; then net='HLV';	fi;
  if [ "$gwname" == 'GW170818' ]; then net='HLV';	fi;

  LI_SKYMAP_DIR="/home/gabriele.vedovato/RESIDUALS/SKYMAP_PUBLIC/GWTC-1_skymaps"
  if ([ $1 == $gwname ] || [ $1 == 'all' ]); then 
    cmd="cp $LI_SKYMAP_DIR/"$gwname"_skymap.fits.gz events/$gwname/data/li_skymap_$net".fits""
    echo $cmd; $cmd
    if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
  fi

done

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

