#!/bin/bash

GW_LIST=("GW150914" "GW151012" "GW151226" "GW170104" "GW170608" "GW170729" "GW170809" "GW170814" "GW170818" "GW170823" "$CWB_GWNAME")

echo ''
echo ' make without parameters -> this help'
echo ''
echo ' --------------------------------------------- '
echo ''
echo ' make case GW=all(default)/event_name/... TYPE='
echo ''
echo '  the list of available cases are:'
echo ''
echo '    PLOT      ->     produce plots'
echo '    HTML      ->     produce html pages'
echo '    CLEAN     ->     remove html pages'
echo ''
echo '  the list of available types are:'
echo ''
echo '    time      ->     produce cWB vs LALInference 90% time (plots or html pages)'
echo '    envelope  ->     produce cWB vs LALInference 90% envelope (plots or html pages)'
echo '    frequency ->     produce cWB vs LALInference 90% frequency (plots or html pages)'
echo '    psd       ->     produce strain sensitivities (plots or html pages)'
echo '    white     ->     produce cWB vs LALInference max-likelihood + whitened data time (plots or html pages)'
echo '    skymap    ->     produce cWB vs LALInference sigle/combined skymaps (plots or html pages)'
echo '    all       ->     produce all (plots or html pages)'
echo ''
echo '  the list of available event names for GW option are:'
echo ''
for gwname in ${GW_LIST[@]} ; do
  echo '    '$gwname
done
echo ''
echo '  by default GW=all -> all events are processed'
echo ''
echo ' --------------------------------------------- '
echo ''
echo ' Examples:'
echo ''
echo ' - time plot for cWB vs LALInference 90% GW150914'
echo ''
echo '     make PLOT  GW=GW150914 TYPE=time'
echo ''
echo ' - html page for cWB vs LALInference 90% GW150914 time plots'
echo ''
echo '     make HTML  GW=GW150914 TYPE=time'
echo ''
echo ' - remove html page for cWB vs LALInference 90% GW150914 time plots'
echo ''
echo '     make CLEAN  GW=GW150914 TYPE=time'
echo ''
echo ' - make skymap plots for GW150914 in batch mode'
echo ''
echo '     make PLOT  GW=GW150914 TYPE=time BATCH=true'
echo ''
echo ' - make skymap plots for all events in batch mode (the plots are produced in parallel)'
echo ''
echo '     make PLOT  GW=all TYPE=time BATCH=true'
echo ''
echo ' - make all plots and html pages for GW150914 + final event html page'
echo ''
echo '     make PLOT  GW=GW150914 TYPE=time'
echo '     make PLOT  GW=GW150914 TYPE=envelope'
echo '     make PLOT  GW=GW150914 TYPE=frequency'
echo '     make PLOT  GW=GW150914 TYPE=white'
echo '     make PLOT  GW=GW150914 TYPE=residuals'
echo '     make PLOT  GW=GW150914 TYPE=skymap'
echo '     make PLOT  GW=GW150914 TYPE=psd'
echo ''
echo '     plot time with plot options (without white spaces)'
echo ''
echo '     make PLOT  GW=GW150914 TYPE=time'' OPTS="'"'"'{"\''"GW190408_181802"''\''",1238782700.0,0.00,0.35,-4.0,4.0,"''\''"down-left"''\''",4}'"'"'"' 
echo ''
echo '     make HTML  GW=GW150914 TYPE=time'
echo '     make HTML  GW=GW150914 TYPE=envelope'
echo '     make HTML  GW=GW150914 TYPE=frequency'
echo '     make HTML  GW=GW150914 TYPE=white'
echo '     make HTML  GW=GW150914 TYPE=residuals'
echo '     make HTML  GW=GW150914 TYPE=skymap'
echo '     make HTML  GW=GW150914 TYPE=psd'
echo ''
echo '     final event html page: OPTS is the sub-title (optional)'
echo ''
echo "     make HTML  GW=GW150914 TYPE=event OPTS="'"'"'( LALInference model = IMRPhenomPv2 )'"'"'
echo ''
echo ' - make time plots and html pages for all events + the final summary html page:'
echo ''
echo '     make PLOT  GW=all TYPE=time'
echo '     make HTML  GW=all TYPE=time'
echo '     make HTML  TYPE=summary_time'
echo ''
echo ' - remove all html event pages + summary time html page'
echo ''
echo '     make CLEAN  GW=all TYPE=time'
echo '     make CLEAN  TYPE=summary_time'
echo ''
echo ' - make all types summary html pages for all events:'
echo ''
echo '     make HTML  TYPE=summary_all'
echo ''
echo ' - make all types html pages for all events:'
echo ''
echo '     make HTML  GW=all TYPE=all'
echo ''
echo ' - make all types plots for all events:'
echo ''
echo '     make PLOT  TYPE=all'
echo ''
echo ' - O3a event data files are stored in the repository directories events/GW_NAME/data'
echo '   The O3a event data files have been copied from directories defined in the configuration file config/Makefile.cwb_pereport_config'
echo '   The commands used to load the GW150914 data files into events/GW150914/data are:'
echo ''
echo '     make COPY  GW=GW150914 TYPE=time'
echo '     make COPY  GW=GW150914 TYPE=envelope'
echo '     make COPY  GW=GW150914 TYPE=frequency'
echo '     make COPY  GW=GW150914 TYPE=white'
echo '     make COPY  GW=GW150914 TYPE=skymap'
echo '     make COPY  GW=GW150914 TYPE=psd'
echo ''
echo ' - In order to define a new event named "USER_GWNAME" not included in the O3a event list we neet to setup from bash the CWB_GWNAME environmental variable'
echo ''
echo '   export CWB_GWNAME="USER_GWNAME"'
echo ''

exit 0

