#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("GW150914" "GW151012" "GW151226" "GW170104" "GW170608" "GW170729" "GW170809" "GW170814" "GW170818" "GW170823" "$CWB_GWNAME")

TYPE_LIST=("time" "envelope" "frequency" "white" "residuals" "psd" "skymap")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkclean.sh par1 par2'
  echo ''
  echo ' par1: available options is "all" or ...'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' par2: available options are "all/summay" or ...'
  echo ''
  for type in ${TYPE_LIST[@]} ; do
    echo ' '$type
  done
  echo ''
  exit 0
fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

if [[ "$2" =~ 'summary' ]]; then
  # remove summary html pages
  if [ $2 == 'summary_skymap' ]; then
    rm -rf  reports/skymap/set1/all
    rm -rf  reports/skymap/set2/all
  fi
  if [ $2 == 'summary_time' ]; then
    rm -rf reports/time/all
  fi
  if [ $2 == 'summary_envelope' ]; then
    rm -rf reports/envelope/all
  fi
  if [ $2 == 'summary_frequency' ]; then
    rm -rf reports/frequency/all
  fi
  if [ $2 == 'summary_white' ]; then
    rm -rf reports/white/all
  fi
  if [ $2 == 'summary_residuals' ]; then
    rm -rf reports/residuals/all
  fi
  if [ $2 == 'summary_psd' ]; then
    rm -rf reports/psd/all
  fi
else
  # remove event html pages
  for gwname in ${GW_LIST[@]} ; do
    if [ "$1" == 'all' ] || [ "$1" == $gwname ]; then
      for type in ${TYPE_LIST[@]} ; do
        if [ "$2" == 'all' ] || [ "$2" == $type ]; then
          echo 'rm -rf events/'$gwname'/report/'$type'/png_html_index'; 
          rm -rf 'events/'$gwname'/report/'$type'/png_html_index'; 
        fi
      done
      cd 'events/'$gwname'/report'; rm -f all; cd -
    fi
  done
fi 

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

