#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("GW150914" "GW151012" "GW151226" "GW170104" "GW170608" "GW170729" "GW170809" "GW170814" "GW170818" "GW170823" "$CWB_GWNAME")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkreport.sh par1 par2 par3 par4 par5'
  echo ''
  echo ' par1: available options are ...'
  echo '       time/envelope/frequency/psd/white/residuals'
  echo ''
  echo ' par2: (optional): available options: true/false(default)'
  echo ''
  echo '       if true the index.html is modified in order to be used for public pages'
  echo ''
fi

if [ "$1" == '' ]; then
  echo ''
  echo ' par1: available options: time/envelope/frequency/psd/white/residuals/"skymap/set1"/skymap/set2"'
  echo ''
  exit 0
fi

if [ "$1" == '' ]; then
  echo ''
  echo ' missing second parameter: available options ...'
  echo ''
  echo ' par2: available options: all/time/envelope/frequency/psd/white/residuals/'skymap/set1'/'skymap/set2''
  echo ''
  exit 0
fi

PUBLIC_INDEX='false'
if [ "$2" != '' ]; then
  PUBLIC_INDEX=$2
fi

HOME_WWW=$(echo "$HOME_WWW" | sed "s/~/\/~/")
HOME_WWW=$(echo "$HOME_WWW" | sed "s/\//\\\\\//g")

# check if cWB is installed
$HOME_CWB/scripts/cwb_watenv.sh
if [ $? != 0 ]; then echo ''; echo 'error: cWB must be installed !!! process terminated'; echo ''; exit 1; fi

if [ "$1" == 'all' ] || [ "$1" == 'time' ]; then
  mkdir -p reports/time/all
  rm reports/time/all/*.png;
  cd reports/time/all;
  for gwname in ${GW_LIST[@]} ; do
    cmd="ln -sf ../../../events/"$gwname"/report/time/rec_signal_time_L1.png "$gwname"_rec_signal_time_L1.png"
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/time/rec_signal_time_H1.png "$gwname"_rec_signal_time_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Waveforms --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/time/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'all' ] || [ "$1" == 'envelope' ]; then
  mkdir -p reports/envelope/all
  rm reports/envelope/all/*.png;
  cd reports/envelope/all;
  for gwname in ${GW_LIST[@]} ; do
    cmd="ln -sf ../../../events/"$gwname"/report/envelope/rec_signal_envelope_L1.png "$gwname"_rec_signal_envelope_L1.png"
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/envelope/rec_signal_envelope_H1.png "$gwname"_rec_signal_envelope_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Waveforms#(envelope) --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/envelope/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'all' ] || [ "$1" == 'frequency' ]; then
  mkdir -p reports/frequency/all
  rm reports/frequency/all/*.png;
  cd reports/frequency/all;
  for gwname in ${GW_LIST[@]} ; do
    cmd="ln -sf ../../../events/"$gwname"/report/frequency/rec_signal_frequency_L1.png "$gwname"_rec_signal_frequency_L1.png"
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/frequency/rec_signal_frequency_H1.png "$gwname"_rec_signal_frequency_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Frequency#Domain#Waveforms --subtitle (cWB#max-likelihood)#vs#(90%#Confidence#Interval:#cWB#Reconstructed#LALInference#Waveforms)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/frequency/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi
  
if [ "$1" == 'all' ] || [ "$1" == 'psd' ]; then
  mkdir -p reports/psd/all
  rm reports/psd/all/*.png;
  cd reports/psd/all;
  for gwname in ${GW_LIST[@]} ; do
    cmd="ln -sf ../../../events/"$gwname"/report/psd/H1_L1_psd.png "$gwname"_psd_H1_L1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Strain#Sensitivities --subtitle O3a#Events#Found#by#cWB'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/psd/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'all' ] || [ "$1" == 'white' ]; then
  mkdir -p reports/white/all
  rm reports/white/all/*.png;
  cd reports/white/all;
  for gwname in ${GW_LIST[@]} ; do
    cmd="ln -sf ../../../events/"$gwname"/report/white/white_time_L1.png "$gwname"_white_time_L1.png"
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/white/white_time_H1.png "$gwname"_white_time_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Waveforms --subtitle (cWB#max-likelihood)#vs#(LALInference#max-likelihood)#vs#(whitened#data#16:512#Hz)'
  #cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Waveforms --subtitle (cWB#max-likelihood)#vs#(whitened#data#16:512#Hz)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/white/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'all' ] || [ "$1" == 'residuals' ]; then
  mkdir -p reports/residuals/all
  rm reports/residuals/all/*.png;
  cd reports/residuals/all;
  for gwname in ${GW_LIST[@]} ; do
    cmd="ln -sf ../../../events/"$gwname"/report/residuals/residuals_time_L1.png "$gwname"_residuals_time_L1.png"
    echo $cmd; $cmd
    cmd="ln -sf ../../../events/"$gwname"/report/residuals/residuals_time_H1.png "$gwname"_residuals_time_H1.png"
    echo $cmd; $cmd
  done
  cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Residuals --subtitle residuals#(LALInference-cWB)#vs#residuals#(data-cWB)'
  #cd ..; $HOME_CWB/scripts/cwb_mkhtml.csh all '--multi true --title Time#Domain#Residuals --subtitle residuals#(cWB)#vs#residuals#(data-cWB)'

  sed -i 's/<table>/<table align="center">/'   all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  all/png_html_index/index.html
  cp ../../html/residuals/index.html .
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/html\//g' index.html
  fi

  cd ../..
fi

if [ "$1" == 'skymap/set1' ] || [ "$1" == 'skymap/set2' ]; then
  mkdir -p reports/$1/all
  rm reports/$1/all/*.png
  cd reports/$1/all;
  for gwname in ${GW_LIST[@]} ; do
    li_net="HL"
    cwb_net="HL"
    if [ "$gwname" == 'GW170729' ]; then li_net='HLV';       fi;
    if [ "$gwname" == 'GW170809' ]; then li_net='HLV';       fi;
    if [ "$gwname" == 'GW170814' ]; then li_net='HLV';       fi;
    if [ "$gwname" == 'GW170817' ]; then li_net='HLV';       fi;
    if [ "$gwname" == 'GW170818' ]; then li_net='HLV';       fi;

    # make cwb HLV skymap report 
    if [ "$li_net" == 'HLV' ] && [ "$1" == 'skymap/set2' ]; then
      cwb_net="HLV"
    fi 
 
    cmd="ln -sf ../../../../events/$gwname/report/skymap/cwb_li_skymap90_"$cwb_net"_"$li_net".png "$gwname"_cwb_li_skymap90_"$cwb_net"_"$li_net".png"
    echo $cmd; $cmd
  done
  cd ../../..; $HOME_CWB/scripts/cwb_mkhtml.csh $1/all '--multi true --title Skymaps --subtitle O3a#Events#Found#by#cWB#(#cWB#vs#LALInference)'

  sed -i 's/<table>/<table align="center">/'   $1/all/png_html_index/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g' $1/all/png_html_index/index.html
  cp ../html/$1/index.html $1/index.html
  sed -i 's/HOME_WWW/'"$HOME_WWW"'/g'  $1/index.html
  if [ $PUBLIC_INDEX == 'true' ]; then
    # the index.html is modified in order to be used for public pages
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/\.\.\/\.\.\/html\//g' $1/all/png_html_index/index.html
    sed -i 's/'"$HOME_WWW"'/\.\.\/\.\.\/\.\.\/html\//g' $1/index.html
  fi

  sed -i 's/<h2>1<\/h2>/<h2>GW150914<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>2<\/h2>/<h2>GW151012<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>3<\/h2>/<h2>GW151226<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>4<\/h2>/<h2>GW170104<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>5<\/h2>/<h2>GW170608<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>6<\/h2>/<h2>GW170729<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>7<\/h2>/<h2>GW170809<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>8<\/h2>/<h2>GW170814<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>9<\/h2>/<h2>GW170818<\/h2>/'  $1/all/png_html_index/index.html
  sed -i 's/<h2>10<\/h2>/<h2>GW170823<\/h2>/' $1/all/png_html_index/index.html

  cd ../..
fi

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

