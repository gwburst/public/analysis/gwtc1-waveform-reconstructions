#!/bin/bash

trap '{ echo "Hey, you pressed Ctrl-C.  Time to quit." ; exit 1; }' INT

GW_LIST=("GW150914" "GW151012" "GW151226" "GW170104" "GW170608" "GW170729" "GW170809" "GW170814" "GW170818" "GW170823" "$CWB_GWNAME")

if [ "$1" == '' ]; then
  echo ''
  echo 'mkcopy.sh par1'
  echo ''
  echo ' par1 (optional) available options ...'
  echo ''
  echo ' default -> all'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  exit 0
fi

# check if $1 is defined in the GW_LIST
CHECK='false'
for gwname in ${GW_LIST[@]} ; do
  if [ "$1" == $gwname ]; then CHECK='true'; fi;
done
if [ "$1" == 'all' ]; then CHECK='true'; fi;
if [ "$CHECK" == 'false' ]; then
  echo ''
  echo "Error: event name $1 doesn't exist in the GW_LIST"
  echo ''
  echo ' available events are:'
  echo ''
  for gwname in ${GW_LIST[@]} ; do
    echo ' '$gwname
  done
  echo ''
  echo ' in order to include the event in the list define from bash the CWB_GWNAME environmental variable'
  echo ''
  echo "  export CWB_GWNAME=$1 "
  echo ''
  exit 1
fi

# copy O3a cwb skymap fits files
for gwname in ${GW_LIST[@]} ; do

  idir='';

  if [ "$gwname" == 'GW170729' ]; then idir='/home/gabriele.vedovato/WLOOK/GWTC-1/GW170729/O2_K19_C02c_LHV_BBH_SIM_ONSPE_GW170729_maxl_wfr_dev1';	   fi;
  if [ "$gwname" == 'GW170809' ]; then idir='/home/gabriele.vedovato/WLOOK/GWTC-1/GW170809/O2_K20_C02c_LHV_BBH_SIM_ONSPE_GW170809_maxl_wfr_dev1';	   fi;
  if [ "$gwname" == 'GW170814' ]; then idir='/home/gabriele.vedovato/WLOOK/GWTC-1/GW170814/O2_K21_C02c_LHV_BBH_SIM_ONSPE_GW170814_maxl_wfr_dev1';	   fi;
  if [ "$gwname" == 'GW170818' ]; then idir='/home/gabriele.vedovato/WLOOK/GWTC-1/GW170818/O2_K21_C02c_LHV_BBH_SIM_ONSPE_GW170818_maxl_wfr_dev1';	   fi;

  if [ "$idir" != '' ] && ([ $1 == $gwname ] || [ $1 == 'all' ]); then 
    cmd="cp $idir/report/ced/*/*/skyprobcc.fits events/$gwname/data/cwb_skymap_HLV.fits"
    echo $cmd; $cmd
    if [ $? != 0 ]; then echo 'error : process terminated'; exit 1; fi
  fi

done

exit 0

trap - INT
echo "One more time, but Ctrl-C should work again."
exit 1

